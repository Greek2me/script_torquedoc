@echo off
::#########
:: TorqueDoc windows zip creator
::#########
:: This file is run to create a zip file in Windows
::
:: Requirements:
:: 7zip (Windows does not include any native form to create a zip file)
:: 7zip path need to be placed in Path environment variable

:: Set up some variables
set ZIP_NAME=Script_TorqueDoc.zip

:: Run the command
7z a -mx0 -tzip -r .\%ZIP_NAME% . -x!*.bat -x!*.rst -x!*.markdown -x!*.*~ -x!*.mkdn -x!*.md -x!.git