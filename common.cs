$Pref::TorqueDoc::allowBlankLine = false; //whether to allow a blank line to be placed after comments
$Pref::TorqueDoc::exportFolder = "config/TorqueDoc";
$Pref::TorqueDoc::exportUncommented = true;
$Pref::TorqueDoc::warnOnPrivateFunctions = true;

exec("./parser.cs");
exec("./tags.cs");

//Documents functions in TorqueScript files and exports them to an HTML page.
//@param	string mask	A mask specifying one or more files. You may use the * wildcard.
//@param	string title	The project title, which will be displayed at the top of the navigation bar.
function document(%mask, %title)
{
	%doc = new scriptGroup()
	{
		class = TorqueDoc;
		projectTitle = (strLen(%title) ? %title : "TorqueDoc");
	};
	%doc.readFO = new fileObject();
	%doc.add(%doc.readFO);
	%doc.writeFO = new fileObject();
	%doc.add(%doc.writeFO);

	%doc.parse(%mask);

	%functions = (%doc.numFunctions $= "" ? 0 : %doc.numFunctions);
	echo("Documented" SPC %functions SPC "functions successfully.");

	%doc.delete();
}