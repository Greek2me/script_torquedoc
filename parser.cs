$TorqueDoc::Error::Tag_AlreadyIncluded = -1;
$TorqueDoc::Error::Tag_NotFound = -2;
$TorqueDoc::Error::None = 1;

//Creates documentation of functions in TorqueScript files and exports it as an HTML page.
//@param	string mask	A mask specifying one or more files. You may use the * wildcard.
//@private
function TorqueDoc::parse(%this,%mask)
{
	%this.numFunctions = 0;

	for(%file = findFirstFile(%mask); %file !$= ""; %file = findNextFile(%mask))
	{
		%this.parseFile(%file);
	}

	%fileName = "/export-" @ stripChars(strReplace(%mask,"/","-")," `~!@#$%^&*()=+[{]}\\|;:\'\",<>/?") @ ".html";
	%this.writeFile($Pref::TorqueDoc::exportFolder @ %fileName);
}

//Parses a specific file and outputs HTML.
//@param	string file	A specific filepath.
//@see	TorqueDoc::parse
//@private
function TorqueDoc::parseFile(%this,%file)
{
	%this.readFO.close();
	%this.readFO.openForRead(%file);

	%lineCount = 0;
	while(!%this.readFO.isEOF())
	{
		%line = %this.readFO.readLine();
		%line = trim(%line);
		%lineCount ++;

		if(strPos(%line,"//") == 0)
		{
			if(!%commentBlockFound)
				%commentBlockFound = true;
			if(%blankLine)
				%blankLine = false;

			%comment = getSubStr(%line,2,strLen(%line));
			%commentTrimmed = lTrim(%comment);

			if(strPos(%commentTrimmed,"@") == 0)
			{
				%tag = getWord(getField(%commentTrimmed,0),0);
				%tag = getSubStr(%tag,1,strLen(%tag));
				%value = getFields(%comment,1);
			}
			else
			{
				%tag = "desc";
				%value = %comment;
			}

			%error = %this.includeTag(%tag,%value);

			if(%error == $TorqueDoc::Error::Tag_AlreadyIncluded)
				warn("WARN: Cannot use multiple tags" SPC %tag);
			if(%error == $TorqueDoc::Error::Tag_NotFound)
				warn("WARN: Tag not found" SPC %tag);
		}
		else if(strLen(%line) || !$Pref::TorqueDoc::allowBlankLine || %blankLine)
		{
			%blankLine = false;

			if(strPos(%line,"function") == 0 && ($Pref::TorqueDoc::exportUncommented || %commentBlockFound))
			{
				%funcDec = %this.parseFunctionDeclaration(%line);
				%this.functionName = getField(%funcDec,0);
				%this.functionParams = getFields(%funcDec,1);
				%this.functionLine = %lineCount;

				%pos = strPos(%this.functionName,"::");
				%pos = (%pos == -1 ? 0 : %pos + 2);
				if(strPos(getSubStr(%this.functionName,%pos,1),"_") == 0)
					%this.includeTag("private");

				%this.html[%this.functionName] = %this.generateHTML_function();

				if(!strLen(%this.numFunctions))
					%this.numFunctions = 0;
				%this.functionName[%this.numFunctions] = %this.functionName;
				%this.numFunctions ++;
			}

			if(%commentBlockFound)
			{
				%commentBlockFound = false;

				for(%i = 0; %i < %this.numTags; %i ++)
				{
					%t = %this.tag[%i];
					for(%e = 0; %e < %this.numTagValues[%t]; %e ++)
					{
						%this.tagValueID[%t,%this.tagValue[%t,%e]] = "";
						%this.tagValue[%t,%e] = "";
					}
					%this.numTagValues[%t] = "";
					%this.tagID[%t] = "";
					%this.tag[%i] = "";
				}
				%this.numTags = "";
			}
		}
		else
		{
			%blankLine = true;
		}
	}

	%this.readFO.close();
}

//This is used to find the function name and parameters
//@param	string dec	The function declaration line.
//@return	string	Returns a tab-delimited list, containing the name of the function and parameters.
//@private
function TorqueDoc::parseFunctionDeclaration(%this,%dec)
{
	if(firstWord(%dec) $= "function")
		%dec = restWords(%dec);

	%name = getSubStr(%dec,0,strPos(%dec,"("));
	%name = trim(%name);

	%startPos = strPos(%dec,"(") + 1;
	%endPos = strPos(%dec,")") - %startPos;
	%paramString = getSubStr(%dec,%startPos,%endPos);

	while(%paramString !$= "")
	{
		%paramString = nextToken(%paramString,"param",",");
		%paramList = setField(%paramList,getFieldCount(%paramList),trim(%param));
	}

	return %name TAB %paramList;
}

//Includes a tag in the current function's description.
//@param	string tag	The type of tag to include.
//@param	string value	The value is then parsed by TorqueDoc::generateHTML_tag[%tag].
//@return	int	Returns error code, or 0 if no error.
//@see	TorqueDoc::parseFile
//@private
function TorqueDoc::includeTag(%this,%tag,%value)
{
	if(!isFunction("TorqueDoc","generateHTML_tag" @ %tag))
		return $TorqueDoc::Error::Tag_NotFound;

	if(!$TorqueDoc::tagMultiple[%tag] && %this.numTagValues[%tag] >= 1)
		return $TorqueDoc::Error::Tag_AlreadyIncluded;

	if(!strLen(%this.numTagValues[%tag]))
	{
		if(!strLen(%this.numTags))
			%this.numTags = 0;
		%this.tag[%this.numTags] = %tag;
		%this.tagID[%tag] = %this.numTags;
		%this.numTags ++;
		%this.numTagValues[%tag] = 0;
	}

	//Let's make sure nobody breaks stuff.
	%value = strReplace(%value,"\"","\\\"");

	%this.tagValue[%tag,%this.numTagValues[%tag]] = %value;

	//Special exception for parameters
	if(%tag $= "param")
	{
		%this.tagParamID[getWord(getField(%value,0),1)] = %this.numTagValues[%tag];
	}

	%this.numTagValues[%tag] ++;

	return 0;
}

//Creates HTML code for the function that is currently being parsed. 
//This should only be called from within TorqueDoc::parseFile.
//@return	string	Properly formatted HTML for a single function.
//@private
function TorqueDoc::generateHTML_function(%this)
{
	//ANCHOR
	%anchor = strReplace(%this.functionName,"::","_SCOPE_");
	%html = "<section><a name=\"" @ %anchor @ "\"></a>";

	//PARAMETERS
	%params = stripChars(%this.functionParams,"%");
	for(%i = 0; %i < getFieldCount(%params); %i ++)
	{
		%par = getField(%params,%i);
		%tagValueID = %this.tagParamID[%par];
		%tagValue = %this.tagValue["param",%tagValueID];

		if(%par $= getWord(%tagValue,1))
			%line = %this.generateHTML_tagParam(%tagValue);
		else
		{
			%type = "var";
			%pos = strPos(%this.functionName,"::");
			if(%i == 0 && %pos > 0)
			{
				%name = getSubStr(%this.functionName,0,%pos);
				if(isFunction(%name,"delete"))
				{
					%type = %name;
				}
			}

			%line = "<div class=\"argument\"><span class=\"type\">" @ %type @ "</span>"
				@ "<span class=\"identifier\">" SPC %par @ "</span></div>";
		}
		%params = setField(%params,%i,%line);
	}
	%params = strReplace(%params,"\t","\n");

	%tagUsed["param"] = true; //we just used the parameter tags

	//FUNCTION BREAKDOWN
	if(%this.numTagValues["return"] > 0)
		%type = getWord(%this.tagValue["return",0],0);
	else
		%type = "void";

	%html = %html @ "<code class=\"function\"><b><span class=\"type\">" @ %type @ "</span>" SPC %this.functionName SPC "</b>("
		NL %params NL ")</code><br /><br />";

	//TAGS
	for(%i = 0; %i < %this.numTags; %i ++)
	{
		%t = %this.tag[%i];
		if($TorqueDoc::tagOrder[%t] != -1)
			%tagList = setField(%tagList,getFieldCount(%tagList),%t);
	}

	while(strLen(%tagList))
	{
		%least = -1;
		for(%i = 0; %i < getFieldCount(%tagList); %i ++)
		{
			%t = getField(%tagList,%i);
			if($TorqueDoc::tagOrder[%t] < %least || %least == -1)
			{
				%least = $TorqueDoc::tagOrder[%t];
				%index = %i;
				%tag = %t;
			}
		}
		%tagList = removeField(%tagList,%index);

		if(%tagUsed[%tag])
			continue;
		%tagUsed[%tag] = true;

		if(isFunction("TorqueDoc","generateHTML_tag" @ %tag))
		{
			if(strLen($TorqueDoc::tagTitle[%tag]))
			{
				if(!%descriptiveListAdded)
				{
					%html = %html NL "<dl>";
					%descriptiveListAdded = true;
				}
				%html = %html NL "<dt>" @ $TorqueDoc::tagTitle[%tag] @ "</dt>";
			}

			for(%i = 0; %i < %this.numTagValues[%tag]; %i ++)
			{
				%val = %this.call("generateHTML_tag" @ %tag,%this.tagValue[%tag,%i]);
				if(strLen(%val))
					%html = %html NL %val;
			}

			if(!strLen(%tagList) && %descriptiveListAdded)
				%html = %html NL "</dl>";
		}
		else
		{
			error("ERROR: Unknown tag in array" SPC %tag);
		}
	}

	//FUNCTION DEFINITION
	%params = strReplace(%this.functionParams,"\t",", ");
	%html = %html NL "<code class=\"definition\"><span class=\"lineno\">" @ %this.functionLine @ "</span>" 
		@ "function" SPC %this.functionName SPC "(" SPC %params SPC ")</code>";

	%html = %html NL "</section>";

	return %html;
}

//Creates HTML for the navigation bar.
//@return	string	The complete navbar HTML.
//@private
function TorqueDoc::generateHTML_navBar(%this)
{
	%sorter = new GuiTextListCtrl();

	%html = "<div id=\"navigation\">\n<header>" @ %this.projectTitle @ "</header>"
		NL "<section id=\"navigationWrapper\">"
		NL "<select id=\"navigationSelect\" onchange=\"location = this.options[this.selectedIndex].value;\">"
		NL "<option disabled>PUBLIC FUNCTIONS</option>";

	for(%i = 0; %i < %this.numFunctions; %i ++)
		%sorter.addRow(%i,%this.functionName[%i]);
	%sorter.sort(0,0);
	for(%i = %sorter.rowCount() - 1; %i >= 0; %i --)
	{
		%func = %sorter.getRowText(%i);
		if(!%this.private[%func])
		{
			%anchor = strReplace(%func,"::","_SCOPE_");
			%html = %html NL "<option value=\"#" @ %anchor @ "\">" @ %func @ "</option>";
			%sorter.removeRow(%i);
		}
	}
	%count = %sorter.rowCount();
	if(%count > 0)
	{
		%html = %html NL "<option disabled></option>"
			NL "<option disabled>PRIVATE FUNCTIONS</option>";

		for(%i = %count - 1; %i >= 0; %i --)
		{
			%func = %sorter.getRowText(%i);
			%anchor = strReplace(%func,"::","_SCOPE_");
			%html = %html NL "<option value=\"#" @ %anchor @ "\">" @ %func @ "</option>";
		}
	}

	%html = %html NL "</select></section></div>";

	%sorter.delete();

	return %html;
}

//Creates the TorqueDoc HTML file.
//@param	string path	The results will be exported to this file.
//@private
function TorqueDoc::writeFile(%this,%path)
{
	%this.writeFO.close();
	%this.writeFO.openForWrite(%path);

	%this.writeFO.writeLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\""
		SPC "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
		NL "<html xmlns=\"http://www.w3.org/1999/xhtml\"><html>");

	%this.writeHeader();
	%this.writeBody();

	%this.writeFO.writeLine("</html>");

	%this.writeFO.close();
	%this.writeFO.delete();
}

//Writes the HTML header.
//@private
function TorqueDoc::writeHeader(%this)
{
	%this.readFO.close();
	%this.readFO.openForRead(expandFileName("./header"));

	while(!%this.readFO.isEOF())
		%this.writeFO.writeLine(%this.readFO.readLine());

	%this.readFO.close();
}

//Writes the HTML body.
//@private
function TorqueDoc::writeBody(%this)
{
	%this.writeFO.writeLine("<body>");

	%navBar = %this.generateHTML_navBar();
	%this.writeFO.writeLine(%navBar);

	%this.writeFO.writeLine("<main>\n<header>Public Functions</header>");
	
	%sorter = new GuiTextListCtrl();
	for(%i = 0; %i < %this.numFunctions; %i ++)
		%sorter.addRow(%i,%this.functionName[%i]);
	%sorter.sort(0,0);
	for(%i = %sorter.rowCount() - 1; %i >= 0; %i --)
	{
		%func = %sorter.getRowText(%i);
		if(!%this.private[%func])
		{
			%this.writeFO.writeLine(%this.html[%func]);
			%sorter.removeRow(%i);
		}
	}

	%count = %sorter.rowCount();
	if(%count > 0)
	{
		%this.writeFO.writeLine("<br /><header>Private & Deprecated Functions</header>");

		for(%i = %count - 1; %i >= 0; %i --)
		{
			%func = %sorter.getRowText(%i);
			%this.writeFO.writeLine(%this.html[%func]);
		}
	}

	%sorter.delete();

	%this.writeFO.writeLine("</main></body>");
}


///Support Functions


//@private
function SimObject::call(%this,%method,%v0,%v1,%v2,%v3,%v4,%v5,%v6,%v7,%v8,%v9,%v10,%v11,%v12,%v13,%v14,%v15,%v16,%v17)
{
	%lastNull = -1;
	for(%i = 0; %i < 18; %i ++)
	{
		%a = %v[%i];
		if(%a $= "")
		{
			if(%lastNull < 0)
				%lastNull = %i;
			continue;
		}
		else
		{
			if(%lastNull >= 0)
			{
				for(%e = %lastNull; %e < %i; %e ++)
				{
					if(%args !$= "")
						%args = %args @ ",";
					%args = %args @ "\"\"";
				}
				%lastNull = -1;
			}
			if(%args !$= "")
				%args = %args @ ",";
			%args = %args @ "\"" @ %a @ "\"";
		}
	}

	return eval(%this @ "." @ %method @ "(" @ %args @ ");");
}