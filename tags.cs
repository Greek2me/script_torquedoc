$TorqueDoc::tag[$TorqueDoc::numTags = 0] = "author";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "deprecated";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "desc";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "link";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "param";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "private";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "return";
$TorqueDoc::tag[$TorqueDoc::numTags ++] = "see";

//Are multiple tags allowed?
$TorqueDoc::tagMultiple["author"] = true;
$TorqueDoc::tagMultiple["deprecated"] = false;
$TorqueDoc::tagMultiple["desc"] = true;
$TorqueDoc::tagMultiple["link"] = true;
$TorqueDoc::tagMultiple["param"] = true;
$TorqueDoc::tagMultiple["private"] = false;
$TorqueDoc::tagMultiple["return"] = false;
$TorqueDoc::tagMultiple["see"] = true;

//Lower values are listed first.
//A value of -1 means that the tag is not listed automatically.
$TorqueDoc::tagOrder["author"] = 600;
$TorqueDoc::tagOrder["deprecated"] = 20;
$TorqueDoc::tagOrder["desc"] = 100;
$TorqueDoc::tagOrder["link"] = 500;
$TorqueDoc::tagOrder["param"] = -1;
$TorqueDoc::tagOrder["private"] = 10;
$TorqueDoc::tagOrder["return"] = 300;
$TorqueDoc::tagOrder["see"] = 400;

//These titles are used for lists.
$TorqueDoc::tagTitle["author"] = "<b>Author:</b>";
$TorqueDoc::tagTitle["deprecated"] = "";
$TorqueDoc::tagTitle["desc"] = "";
$TorqueDoc::tagTitle["link"] = "<b>External Links:</b>";
$TorqueDoc::tagTitle["param"] = "";
$TorqueDoc::tagTitle["private"] = "";
$TorqueDoc::tagTitle["return"] = "<b>Returns:</b>";
$TorqueDoc::tagTitle["see"] = "<b>See Also:</b>";

//Parameters must either be one of these types or be a valid class name.
$TorqueDoc::paramType[$TorqueDoc::numParamTypes = 0] = "bool";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "datablock";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "float";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "int";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "point2F";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "point2I";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "point3F";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "point3I";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "string";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "transform";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "vector2F";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "vector2I";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "vector3F";
$TorqueDoc::paramType[$TorqueDoc::numParamTypes ++] = "vector3I";

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagAuthor(%this,%value)
{
	%html = "<dd>" @ %value @ "</dd>";
	return %html;
}

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagDeprecated(%this,%value)
{
	//mark it as private - we don't want people using this
	%this.private[%this.functionName] = true;

	%html = "<section class=\"warningBox\" style=\"margin-top:0px;margin-bottom:16px;\"><b>Deprecated:</b>"
		SPC "It is recommended that you do not use this function."
		SPC "It may be removed at a later date.</section>";
	return %html;
}

//The desc tag isn't actually a tag - it's the description.
//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagDesc(%this,%value)
{
	%html = "<p>" @ %value @ "</p>";
	return %html;
}

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagLink(%this,%value)
{
	%url = getField(%value,0);
	%text = getField(%value,1);
	if(!strLen(%text))
		%text = %url;
	%html = "<dd><a href=\"" @ %url @ "\" target=\"_blank\"><code>" @ %text @ "</code></a></dd>";
	return %html;
}

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagParam(%this,%value)
{
	%param = getField(%value,0);
	%type = getWord(%param,0);
	%name = getWord(%param,1);
	%comment = getField(%value,1);

	if(!strLen(%name))
	{
		warn("WARN: Invalid parameter. Separate parameter type and name with a space.");
		return;
	}

	for(%i = 0; %i < $TorqueDoc::numParamTypes; %i ++)
	{
		if($TorqueDoc::paramType[%i] $= %type)
		{
			%typeFound = true;
			break;
		}
	}
	if(!%typeFound && !isFunction(%type,"delete"))
	{
		warn("WARN: Unknown parameter type" SPC %type);
		return;
	}

	%html = "<div class=\"argument hangingIndent\"><span class=\"type\">" @ %type @ "</span>"
		@ "<span class=\"identifier\">" SPC %name @ "</span>";

	if(strLen(%comment))
		%html = %html SPC "&mdash;" SPC %comment @ "</div>";
	else
		%html = %html @ "</div>";

	return %html;
}

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagPrivate(%this,%value)
{
	%this.private[%this.functionName] = true;

	if($Pref::TorqueDoc::warnOnPrivateFunctions)
	{
		%html = "<section class=\"warningBox\" style=\"margin-top:0px;margin-bottom:16px;\"><b>Private:</b>"
			SPC "Do not call this function."
			SPC "There is probably a public function for you to use.</section>";
	}
	else
		%html = "";
	return %html;
}

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagReturn(%this,%value)
{
	%type = getField(%value,0);
	%comment = getField(%value,1);

	for(%i = 0; %i < $TorqueDoc::numParamTypes; %i ++)
	{
		if($TorqueDoc::paramType[%i] $= %type)
		{
			%typeFound = true;
			break;
		}
	}
	if(!%typeFound && !isFunction(%type,"delete"))
	{
		warn("WARN: Unknown return type" SPC %type);
		return;
	}

	%html = "<dd>" @ %comment @ "</dd>";

	return %html;
}

//@param	string value	The value of the tag.
//@return	string	The complete HTML for the tag.
//@private
function TorqueDoc::generateHTML_tagSee(%this,%value)
{
	%anchor = strReplace(%value,"::","_SCOPE_");
	%html = "<dd><a href=\"#" @ %anchor @ "\"><code>" @ %value @ "</code></a></dd>";
	return %html;
}